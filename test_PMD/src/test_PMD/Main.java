package test_PMD;
import java.util.*;
/**
 * Class Principal Main
 * @author xelbol
 *
 */
public class Main {
	/**
	 * Class static main 
	 * @param args
	 */
	public static void main(final String [] args) {
		final Scanner teclado = new Scanner(System.in);
	    System.out.print("Ingresa un numero: ");
	    int numero = teclado.nextInt();
	    while(numero<1){
	      System.out.print("Error! ingrese un numero mayor a 0: ");
	      numero = teclado.nextInt();
	    }
	    System.out.print("Ingrese un caracter: ");
	    final String caracter = teclado.next();
	    int contadorfila = 0; 
	    while(contadorfila<numero){ 
	      int contadorcolumna = 0;  
	      while(contadorcolumna<numero){ 
	        System.out.print(caracter);
	        contadorcolumna++;
	      }
	      System.out.println();
	      contadorfila++;
	    }
	}
}
